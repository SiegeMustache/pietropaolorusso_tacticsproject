﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : Singleton<StatsManager>
{

    GameObject[] redTeamContainer;
    GameObject[] blueTeamContainer;

    public PlayerSave[] redTeamPawnsBackUp;
    public PlayerSave[] blueTeamPawnsBackUp;

    private void Awake()
    {
        if (FindObjectsOfType<StatsManager>().Length > 1)
        {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void SaveGameStats()
    {
        redTeamContainer = GameObject.FindGameObjectsWithTag("PlayerRed");
        blueTeamContainer = GameObject.FindGameObjectsWithTag("PlayerBlue");
        //redTeamContainer = GameManager.instance.redTeamPawns;
        //blueTeamContainer = GameManager.instance.blueTeamPawns;
        redTeamPawnsBackUp = new PlayerSave[redTeamContainer.Length];
        blueTeamPawnsBackUp = new PlayerSave[blueTeamContainer.Length];

        for (int i = 0; i < redTeamContainer.Length; i++)
        {
            PlayerManager redBackUp = redTeamContainer[i].GetComponent<PlayerManager>();
            Debug.Log(redBackUp);
            Debug.Log(redTeamPawnsBackUp[i]);
            redTeamPawnsBackUp[i] = new PlayerSave();
            redTeamPawnsBackUp[i].combatRange = redBackUp.combatRange;
            redTeamPawnsBackUp[i].gold = redBackUp.gold;
            redTeamPawnsBackUp[i].maximumHealth = redBackUp.maximumHealth;
            redTeamPawnsBackUp[i].movementRange = redBackUp.movementRange;
            redTeamPawnsBackUp[i].pawnClass = redBackUp.currentPawnClass;
            redTeamPawnsBackUp[i].strength = redBackUp.strength;
        }
        for (int i = 0; i < blueTeamContainer.Length; i++)
        {
            PlayerManager blueBackUp = blueTeamContainer[i].GetComponent<PlayerManager>();
            blueTeamPawnsBackUp[i] = new PlayerSave();
            blueTeamPawnsBackUp[i].combatRange = blueBackUp.combatRange;
            blueTeamPawnsBackUp[i].gold = blueBackUp.gold;
            blueTeamPawnsBackUp[i].maximumHealth = blueBackUp.maximumHealth;
            blueTeamPawnsBackUp[i].movementRange = blueBackUp.movementRange;
            blueTeamPawnsBackUp[i].strength = blueBackUp.strength;
            blueTeamPawnsBackUp[i].pawnClass = blueBackUp.currentPawnClass;
        }
    }

    public void DestroyStats()
    {
        redTeamContainer = null;
        blueTeamContainer = null;
        redTeamPawnsBackUp = null;
        blueTeamPawnsBackUp = null;
    }
}

public class PlayerSave
{
    public int gold;
    public int strength;
    public int movementRange;
    public int combatRange;
    public int maximumHealth;
    public PlayerManager.PawnClass pawnClass;
}