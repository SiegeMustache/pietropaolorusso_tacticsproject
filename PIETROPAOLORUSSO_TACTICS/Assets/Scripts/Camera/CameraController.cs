﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CameraController : MonoBehaviour {

    public GameObject spot1Obj;
    public GameObject spot2Obj;
    public GameObject spot3Obj;
    public GameObject spot4Obj;

    Vector3 spot1Pos;
    Vector3 spot2Pos;
    Vector3 spot3Pos;
    Vector3 spot4Pos;
    Vector3 currentPos;

    Quaternion spot1Rot;
    Quaternion spot2Rot;
    Quaternion spot3Rot;
    Quaternion spot4Rot;

    bool spot1Flag;
    bool spot2Flag;
    bool spot3Flag;
    bool spot4Flag;
    public bool isMovingLeft;
    public bool isMovingRight;

    public float cameraTravelTime;
    public float cameraSpeed;

    private float startTime;
    private float journeyLength1;
    private float journeyLength2;
    private float journeyLength3;
    private float journeyLength4;
    private float journeyLength1Inverse;
    private float journeyLength2Inverse;
    private float journeyLength3Inverse;
    private float journeyLength4Inverse;
    
    public static Action SpawnCameraMoveSource;
    public static Action StopCameraMoveSource;

    // Use this for initialization
    void Start () {

        spot1Pos = spot1Obj.GetComponent<Transform>().position;
        spot2Pos = spot2Obj.GetComponent<Transform>().position;
        spot3Pos = spot3Obj.GetComponent<Transform>().position;
        spot4Pos = spot4Obj.GetComponent<Transform>().position;

        spot1Rot = spot1Obj.GetComponent<Transform>().rotation;
        spot2Rot = spot2Obj.GetComponent<Transform>().rotation;
        spot3Rot = spot3Obj.GetComponent<Transform>().rotation;
        spot4Rot = spot4Obj.GetComponent<Transform>().rotation;

        transform.position = spot1Pos;
        transform.rotation = spot1Rot;

        currentPos = transform.position;

        startTime = Time.time;
        journeyLength1 = Vector3.Distance(spot1Pos, spot2Pos);
        journeyLength2 = Vector3.Distance(spot2Pos, spot3Pos);
        journeyLength3 = Vector3.Distance(spot3Pos, spot4Pos);
        journeyLength4 = Vector3.Distance(spot4Pos, spot1Pos);

        journeyLength1Inverse = Vector3.Distance(spot2Pos, spot1Pos);
        journeyLength2Inverse = Vector3.Distance(spot3Pos, spot2Pos);
        journeyLength3Inverse = Vector3.Distance(spot4Pos, spot3Pos);
        journeyLength4Inverse = Vector3.Distance(spot1Pos, spot4Pos);

    }
	
	// Update is called once per frame
	void Update () {

        currentPos = transform.position;

        if(currentPos == spot1Pos)
        {
            spot1Flag = true;
            spot2Flag = false;
            spot3Flag = false;
            spot4Flag = false;
        }
        else if(currentPos == spot2Pos)
        {
            spot1Flag = false;
            spot2Flag = true;
            spot3Flag = false;
            spot4Flag = false;
        }
        else if(currentPos == spot3Pos)
        {
            spot1Flag = false;
            spot2Flag = false;
            spot3Flag = true;
            spot4Flag = false;
        }
        else if(currentPos == spot4Pos)
        {
            spot1Flag = false;
            spot2Flag = false;
            spot3Flag = false;
            spot4Flag = true;
        }

        //TURN LEFT
        if (spot1Flag == true && spot2Flag == false && spot3Flag == false && spot4Flag == false && isMovingLeft == true)
        {
            float distCovered = (Time.time - startTime) * cameraSpeed;
            float fracJourney = distCovered / journeyLength4Inverse;
            transform.position = Vector3.Lerp(spot1Pos, spot4Pos, fracJourney);
            transform.rotation = Quaternion.Lerp(spot1Rot, spot4Rot, fracJourney);
            if(transform.position == spot4Pos)
            {
                isMovingLeft = false;
            }
        }
        else if (spot1Flag == false && spot2Flag == true && spot3Flag == false && spot4Flag == false && isMovingLeft == true)
        {
            float distCovered = (Time.time - startTime) * cameraSpeed;
            float fracJourney = distCovered / journeyLength1Inverse;
            transform.position = Vector3.Lerp(spot2Pos, spot1Pos, fracJourney);
            transform.rotation = Quaternion.Lerp(spot2Rot, spot1Rot, fracJourney);
            if(transform.position == spot1Pos)
            {
                isMovingLeft = false;
            }
        }
        else if (spot1Flag == false && spot2Flag == false && spot3Flag == true && spot4Flag == false && isMovingLeft == true)
        {
            float distCovered = (Time.time - startTime) * cameraSpeed;
            float fracJourney = distCovered / journeyLength2Inverse;
            transform.position = Vector3.Lerp(spot3Pos, spot2Pos, fracJourney);
            transform.rotation = Quaternion.Lerp(spot3Rot, spot2Rot, fracJourney);
            if(transform.position == spot2Pos)
            {
                isMovingLeft = false;
            }
        }
        else if (spot1Flag == false && spot2Flag == false && spot3Flag == false && spot4Flag == true && isMovingLeft == true)
        { 
            float distCovered = (Time.time - startTime) * cameraSpeed;
            float fracJourney = distCovered / journeyLength3Inverse;
            transform.position = Vector3.Lerp(spot4Pos, spot3Pos, fracJourney);
            transform.rotation = Quaternion.Lerp(spot4Rot, spot3Rot, fracJourney);
            if(transform.position == spot3Pos)
            {
                isMovingLeft = false;
            }
        }

        //TURN RIGHT
        if (spot1Flag == true && spot2Flag == false && spot3Flag == false && spot4Flag == false && isMovingRight == true)
        {
            float distCovered = (Time.time - startTime) * cameraSpeed;
            float fracJourney = distCovered / journeyLength1;
            transform.position = Vector3.Lerp(spot1Pos, spot2Pos, fracJourney);
            transform.rotation = Quaternion.Lerp(spot1Rot, spot2Rot, fracJourney);
            if(transform.position == spot2Pos)
            {
                isMovingRight = false;
            }
        }
        else if (spot1Flag == false && spot2Flag == true && spot3Flag == false && spot4Flag == false && isMovingRight == true)
        {
            float distCovered = (Time.time - startTime) * cameraSpeed;
            float fracJourney = distCovered / journeyLength2;
            transform.position = Vector3.Lerp(spot2Pos, spot3Pos, fracJourney);
            transform.rotation = Quaternion.Lerp(spot2Rot, spot3Rot, fracJourney);
            if(transform.position == spot3Pos)
            {
                isMovingRight = false;
            }
        }
        else if (spot1Flag == false && spot2Flag == false && spot3Flag == true && spot4Flag == false && isMovingRight == true)
        {
            float distCovered = (Time.time - startTime) * cameraSpeed;
            float fracJourney = distCovered / journeyLength3;
            transform.position = Vector3.Lerp(spot3Pos, spot4Pos, fracJourney);
            transform.rotation = Quaternion.Lerp(spot3Rot, spot4Rot, fracJourney);
            if(transform.position == spot4Pos)
            {
                isMovingRight = false;
            }
        }
        else if (spot1Flag == false && spot2Flag == false && spot3Flag == false && spot4Flag == true && isMovingRight == true)
        {
            float distCovered = (Time.time - startTime) * cameraSpeed;
            float fracJourney = distCovered / journeyLength4;
            transform.position = Vector3.Lerp(spot4Pos, spot1Pos, fracJourney);
            transform.rotation = Quaternion.Lerp(spot4Rot, spot1Rot, fracJourney);
            if (transform.position == spot1Pos)
            {
                isMovingRight = false;
            }
        }

    }

    public void TurnLeft()
    {
        SoundManager.instance.Play(SoundManager.instance.buttonSound, false);
        if(spot1Flag == true && spot2Flag == false && spot3Flag == false && spot4Flag == false && isMovingLeft == false)
        {
            startTime = Time.time;
            isMovingLeft = true;
        }
        else if (spot1Flag == false && spot2Flag == true && spot3Flag == false && spot4Flag == false && isMovingLeft == false)
        {
            startTime = Time.time;
            isMovingLeft = true;
        }
        else if (spot1Flag == false && spot2Flag == false && spot3Flag == true && spot4Flag == false && isMovingLeft == false)
        {
            startTime = Time.time;
            isMovingLeft = true;
        }
        else if (spot1Flag == false && spot2Flag == false && spot3Flag == false && spot4Flag == true && isMovingLeft == false)
        {
            startTime = Time.time;
            isMovingLeft = true;
        }
    }

    public void TurnRight()
    {
        SoundManager.instance.Play(SoundManager.instance.buttonSound, false);
        if (spot1Flag == true && spot2Flag == false && spot3Flag == false && spot4Flag == false && isMovingRight == false)
        {
            startTime = Time.time;
            isMovingRight = true;
        }
        else if (spot1Flag == false && spot2Flag == true && spot3Flag == false && spot4Flag == false && isMovingRight == false)
        {
            startTime = Time.time;
            isMovingRight = true;
        }
        else if (spot1Flag == false && spot2Flag == false && spot3Flag == true && spot4Flag == false && isMovingRight == false)
        {
            startTime = Time.time;
            isMovingRight = true;
        }
        else if (spot1Flag == false && spot2Flag == false && spot3Flag == false && spot4Flag == true && isMovingRight == false)
        {
            startTime = Time.time;
            isMovingRight = true;
        }
    }
}
