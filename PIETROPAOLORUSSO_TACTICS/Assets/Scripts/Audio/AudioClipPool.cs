﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipPool {
    Queue<AudioSource> AvailableSource = new Queue<AudioSource>();
    public List<AudioSource> ActiveSource = new List<AudioSource>();

    public AudioClipPool(int PoolWidth, GameObject PoolPrefab, Transform Parent)
    {
        for(int i = 0; i < PoolWidth; i++)
        {

            GameObject NewAudioSource = GameObject.Instantiate(PoolPrefab, Parent);
            AudioSource ObjectSource = NewAudioSource.GetComponent<AudioSource>();
            NewAudioSource.SetActive(false);
            AvailableSource.Enqueue(ObjectSource);
        }
    }

    public AudioSource Fetch()
    {
        if(AvailableSource.Count <= 0)
        {
            return null;
        }

        AudioSource SourceToActive = AvailableSource.Dequeue();
        SourceToActive.gameObject.SetActive(true);
        ActiveSource.Add(SourceToActive);
        return SourceToActive;
    }

    public void Push (AudioSource source)
    {
        if (ActiveSource.Contains(source))
        {
            ActiveSource.Remove(source);
            source.gameObject.SetActive(false);
            AvailableSource.Enqueue(source);
        }
    }
	
}
