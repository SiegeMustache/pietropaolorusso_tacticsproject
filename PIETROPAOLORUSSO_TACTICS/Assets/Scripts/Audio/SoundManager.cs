﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager> {

    public GameObject ClipPrefab;

    public AudioClip warriorHit;
    public AudioClip[] warriorMoveQuotes;
    public AudioClip[] warriorAttackWarriorQuotes;
    public AudioClip[] warriorDeathQuotes;

    public AudioClip[] archerMoveQuotes;
    public AudioClip[] archerAttackArcherQuotes;
    public AudioClip[] archerDeathQuotes;

    public AudioClip music;
    public AudioClip buyShopSound;
    public AudioClip openShopSound;
    public AudioClip closeShopSound;
    public AudioClip healSound;
    public AudioClip buttonSound;
    public AudioClip victory;

    AudioClipPool Pool;


    private void Start()
    {
        Pool = new AudioClipPool(25, ClipPrefab, this.transform);
        Play(music, true);
    }
    public void Play(AudioClip clip, bool loop)
    {
        AudioSource source = Pool.Fetch();
        if (source != null)
        {
            source.clip = clip;
            source.loop = loop;
            source.Play();
        }
    }

    public void Stop(AudioClip clip)
    {
        foreach (AudioSource source in Pool.ActiveSource)
        {
            Queue<AudioSource> SourcesToStop = new Queue<AudioSource>();
            if (source.clip == clip)
            {
                source.Stop();
                SourcesToStop.Enqueue(source);
            }

            while (SourcesToStop.Count > 0)
            {
                Pool.Push(SourcesToStop.Dequeue());
            }
        }
    }

    void Update()
    {
        Queue<AudioSource> SourcesToStop = new Queue<AudioSource>();
        foreach (AudioSource source in Pool.ActiveSource)
        {
            if (source.time >= source.clip.length)
            {
                SourcesToStop.Enqueue(source);
            }
        }
        while (SourcesToStop.Count > 0)
        {
            Pool.Push(SourcesToStop.Dequeue());
        }

    }
}
