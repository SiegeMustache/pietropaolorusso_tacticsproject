﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TargetPlayer : MonoBehaviour {

    Camera camera;

    public Sprite swordMaster;
    public Sprite archer;

    public Text classText;
    public Text hpText;
    public Text strengthText;
    public Text goldText;
    public Text movementText;
    public Image panel;
    public Image classIcon;
    public GameObject myPanel;

    // Use this for initialization
    void Start () {

        camera = Camera.main;

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit))
        {
            if(hit.collider.tag == "PlayerBlue")
            {
                myPanel.SetActive(true);
                PlayerManager targetPlayer = hit.collider.GetComponent<PlayerManager>();

                if(targetPlayer.currentPawnClass == PlayerManager.PawnClass.SwordMaster)
                {
                    classIcon.sprite = swordMaster;
                    classText.text = "Sword Master";
                }
                else if(targetPlayer.currentPawnClass == PlayerManager.PawnClass.Archer)
                {
                    classIcon.sprite = archer;
                    classText.text = "Archer";
                }

                panel.color = Color.blue;
                hpText.text = targetPlayer.currentHealth.ToString();
                strengthText.text = targetPlayer.strength.ToString();
                goldText.text = targetPlayer.strength.ToString();
                movementText.text = targetPlayer.movementRange.ToString();
            }
            else if(hit.collider.tag == "PlayerRed")
            {
                myPanel.SetActive(true);
                PlayerManager targetPlayer = hit.collider.GetComponent<PlayerManager>();

                if(targetPlayer.currentPawnClass == PlayerManager.PawnClass.SwordMaster)
                {
                    classIcon.sprite = swordMaster;
                    classText.text = "Sword Master";
                }
                else if(targetPlayer.currentPawnClass == PlayerManager.PawnClass.Archer)
                {
                    classIcon.sprite = archer;
                    classText.text = "Archer";
                }

                panel.color = Color.red;
                hpText.text = targetPlayer.currentHealth.ToString();
                strengthText.text = targetPlayer.strength.ToString();
                goldText.text = targetPlayer.strength.ToString();
                movementText.text = targetPlayer.movementRange.ToString();
            }
            else
            {
                myPanel.SetActive(false);
            }
        }

	}
}
