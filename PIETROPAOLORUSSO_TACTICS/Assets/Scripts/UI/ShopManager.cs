﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShopManager : Singleton<ShopManager> {

    public GameObject shopAccess;
    public GameObject shop;
    public GameObject attackRangeStat;
    public GameObject movementRangeStat;

    public Sprite swordIcon;
    public Sprite archerIcon;

    public Text classText;
    public Image classIcon;
    public Text currentGold;
    public Text currentHealthText;
    public Text upgradedHealthText;
    public Text currentStrengthText;
    public Text upgradedStrengthText;
    public Text currentMovementText;
    public Text upgradedMovementText;
    public Text currentRangeText;
    public Text upgradedRangeText;

    public int upgradeCost = 100;
    public bool shopOpen = false;

    PlayerManager currentPlayer;
	// Use this for initialization
	void Start () {
        currentPlayer = GameManager.instance.currentPlayer;
        shopAccess.SetActive(true);
        shop.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        currentPlayer = GameManager.instance.currentPlayer;
		if(shopOpen == true)
        {
            if(currentPlayer.currentPawnClass == PlayerManager.PawnClass.Archer)
            {
                attackRangeStat.SetActive(true);
                movementRangeStat.SetActive(false);
                classIcon.sprite = archerIcon;
                classText.text = "Archer";
            }
            else if(currentPlayer.currentPawnClass == PlayerManager.PawnClass.SwordMaster)
            {
                attackRangeStat.SetActive(false);
                movementRangeStat.SetActive(true);
                classIcon.sprite = swordIcon;
                classText.text = "Sword Master";
            }
        }

        currentHealthText.text = currentPlayer.currentHealth.ToString() + "/" + currentPlayer.maximumHealth.ToString();
        upgradedHealthText.text = currentPlayer.currentHealth.ToString() + "/" + (currentPlayer.maximumHealth + 100).ToString();
        currentStrengthText.text = currentPlayer.strength.ToString();
        upgradedStrengthText.text = (currentPlayer.strength + 50).ToString();
        currentMovementText.text = currentPlayer.movementRange.ToString();
        upgradedMovementText.text = (currentPlayer.movementRange + 1).ToString();
        currentRangeText.text = currentPlayer.combatRange.ToString();
        upgradedRangeText.text = (currentPlayer.combatRange + 1).ToString();
        currentGold.text = currentPlayer.gold.ToString();

        
	}

    public void CloseShop()
    {
        if (shopOpen == true)
        {
            shopAccess.SetActive(true);
            shop.SetActive(false);
            shopOpen = false;
            SoundManager.instance.Play(SoundManager.instance.closeShopSound, false);
        }
    }

    public void AccessShop()
    {
        shopAccess.SetActive(false);
        shop.SetActive(true);
        shopOpen = true;
        SoundManager.instance.Play(SoundManager.instance.openShopSound, false);
    }

    public void IncreaseHealth()
    {
        if(currentPlayer.gold >= upgradeCost)
        {
            currentPlayer.ConsumeGold(upgradeCost);
            currentPlayer.IncreaseHealth(100);
            PlayBuySound();
        }
    }

    public void IncreaseStrength()
    {
        if(currentPlayer.gold >= upgradeCost)
        {
            currentPlayer.ConsumeGold(upgradeCost);
            currentPlayer.IncreaseStrength(50);
            PlayBuySound();
        }
    }

    public void IncreaseMovement()
    {
        if(currentPlayer.gold >= upgradeCost)
        {
            currentPlayer.ConsumeGold(upgradeCost);
            currentPlayer.IncreaseMovement(1);
            PlayBuySound();
        }
    }

    public void IncreaseCombatRange()
    {
        if(currentPlayer.gold >= upgradeCost)
        {
            currentPlayer.ConsumeGold(upgradeCost);
            currentPlayer.IncreaseAttackRange(1);
            PlayBuySound();
        }
    }

    public void Heal()
    {
        if(currentPlayer.gold >= upgradeCost && currentPlayer.currentHealth < currentPlayer.maximumHealth)
        {
            currentPlayer.ConsumeGold(upgradeCost);
            currentPlayer.Heal(100);
            SoundManager.instance.Play(SoundManager.instance.healSound, false);
        }
    }

    private void PlayBuySound()
    {
        SoundManager.instance.Play(SoundManager.instance.buyShopSound, false);
    }
}
