﻿using UnityEngine;
using System.Collections.Generic;

public class Node
{
    public List<Node> neighbours;

    public int x;
    public int z;

    public GameObject tile;

    public bool isCurrentPath = false;
    public bool isAvailable = false;
    public bool attackAvailable = false;
    public bool isWalkable = true;

    public Color baseColor;

    public Vector3 pathPosition;
    public int movementWeight;
    public int baseMovementWeight;

    public Node()
    {
        neighbours = new List<Node>();
    }

    public float DistanceTo(Node n)
    {
        if (n == null)
        {
            Debug.LogError("The tile distance is between the tile itself or the tile selected is nonexistent");
        }

        return Vector3.Distance(new Vector3(x, z),new Vector3(n.x, n.z));
    }
}
